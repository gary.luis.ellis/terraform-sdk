package scaffold

import (
	"path/filepath"

	"github.com/garyellis/terraformer-sdk/pkg/scaffold/input"
)

const tfenvfile = ".terraform-version"

type TfEnv struct {
	input.Input

	TerraformVersion string
}

func (s *TfEnv) GetInput() (input.Input, error) {
	if s.Path == "" {
		s.Path = filepath.Join("./", tfenvfile)
	}
	s.TemplateBody = tfenvTmpl
	return s.Input, nil
}

const tfenvTmpl = `{{.TerraformVersion}}
`

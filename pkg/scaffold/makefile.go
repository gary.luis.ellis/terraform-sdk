package scaffold

import (
	"path/filepath"

	"github.com/garyellis/terraformer-sdk/pkg/scaffold/input"
)

const Makefilefile = "Makefile"

type Makefile struct {
	input.Input
}

func (s *Makefile) GetInput() (input.Input, error) {
	if s.Path == "" {
		s.Path = filepath.Join("./", Makefilefile)
	}
	s.TemplateBody = makefileTmpl
	return s.Input, nil
}

const makefileTmpl = `
source_global_env := source $(env_name)-terraform.env
source_module_env := if [ -e "./module.env" ]; then source ./module.env ; fi
exec_pre_module_script := if [ -e "./pre.sh" ]; then ./pre.sh ; fi
exec_post_module_script := if [ -e "./post.sh" ]; then ./post.sh ; fi

setup_backend := ../scripts/setup-backend.sh

define init_and_plan
    $(source_global_env) && cd $(1) && $(setup_backend) && terraform init && $(source_module_env) && $(exec_pre_module_script) && terraform plan
endef

define init_and_apply
    $(source_global_env) && cd $(1) && $(setup_backend) && terraform init && $(source_module_env) && $(exec_pre_module_script) && terraform apply -auto-approve && $(exec_post_module_script)
endef

define destroy
    $(source_global_env) && cd $(1) && $(setup_backend) terraform init && $(source_module_env) && terraform destroy -force
endef


plan:
	#

apply:
	#

destroy:
	#
`

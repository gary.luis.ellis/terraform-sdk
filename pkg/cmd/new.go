package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/garyellis/terraformer-sdk/pkg/scaffold"
	"github.com/garyellis/terraformer-sdk/pkg/scaffold/input"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	stackName        string
	terraformVersion string
)

func NewNewCmd() *cobra.Command {
	newStackCmd := &cobra.Command{
		Use:   "new <stack name>",
		Short: "",
		Long: `The terraformer-sdk new command creates a new terraform stack project and generates
		 a default directory layout based on the input <stack-name>.`,
		RunE: newStack,
	}

	newStackCmd.Flags().StringVar(&terraformVersion, "terraform-version", "", "The terraform version")
	return newStackCmd
}

func newStack(cmd *cobra.Command, args []string) error {
	if err := parse(cmd, args); err != nil {
		return err
	}

	log.Info("Creating new terraform stack '%s'", strings.Title(stackName))
	cfg := &input.Config{
		Repo:           stackName,
		AbsProjectPath: filepath.Join(MustGetwd(), stackName),
	}

	s := scaffold.Scaffold{}
	err := s.Execute(
		cfg,
		&scaffold.Makefile{},
		&scaffold.TfEnv{TerraformVersion: terraformVersion},
	)
	if err != nil {
		log.Fatalf("new terraform stack project creation failed: (%v)", err)
	}
	log.Info("terraform stack project creation complete.")
	return nil
}

func parse(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("command %s requires exactly one argument", cmd.CommandPath())
	}
	stackName = args[0]
	if len(stackName) == 0 {
		return fmt.Errorf("project name must not be empty")
	}
	return nil
}

// Gets the working directory
func MustGetwd() string {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("Failed to get working directory: (%v)", err)
	}
	return wd
}

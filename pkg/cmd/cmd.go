package cmd

import "github.com/spf13/cobra"

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "terraformer-sdk",
		Short:   "Generate terraform stack scaffolding",
		Version: "0.0.0",
	}

	cmd.AddCommand(NewNewCmd())
	return cmd
}
